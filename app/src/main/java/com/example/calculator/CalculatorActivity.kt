package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_calculator.*

class CalculatorActivity : AppCompatActivity() ,View.OnClickListener {

    private var variableFirst = 0.0
    private var variableSecond = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
        init()
    }
    private fun init(){
     button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        buttonDot.setOnClickListener(this)
    }

    fun equal(view: View){
    val value = resulttextview.text.toString()
    if (value.isNotEmpty()){
        variableSecond = value.toDouble()
        operation()
    }
    }

    private fun operation(){
        var result = 0.0
        if(operation ==  ":"){
            result = variableFirst / variableSecond
            resulttextview.text = result.toString()
        }else if (operation == "+"){
            result = variableFirst + variableSecond
            resulttextview.text = result.toString()
        }else if (operation == "x"){
            result = variableFirst * variableSecond
            resulttextview.text = result.toString()
        }else if (operation == "-"){
            result = variableFirst - variableSecond
            resulttextview.text = result.toString()
        }

    }

    fun plus(view: View){
        val value = resulttextview.text.toString()
        if (value.isNotEmpty()) {
            variableFirst = resulttextview.text.toString().toDouble()
            operation = "+"
            resulttextview.text = ""
        }

    }

    fun times(view: View){
        val value = resulttextview.text.toString()
        if (value.isNotEmpty()) {
            variableFirst = resulttextview.text.toString().toDouble()
            operation = "x"
            resulttextview.text = ""
        }
    }

    fun minus(view: View) {
        val value: String = resulttextview.text.toString()
        if (value.isNotEmpty()) {
            variableFirst = value.toDouble()
            operation = "+"
            resulttextview.text = ""
        }
    }


    fun divide(view: View){
        val value = resulttextview.text.toString()
        if (value.isNotEmpty())
            variableFirst = resulttextview.text.toString().toDouble()
            operation = ":"
            resulttextview.text = ""

    }

    fun delete(view:View){
        val value = resulttextview.text.toString()
        if (value.isNotEmpty())
        resulttextview.text = value.substring(0, value.length - 1)
    }

    override fun onClick(v: View?) {
        val button : Button = v as Button
        resulttextview.text = resulttextview.text.toString() + button.text.toString()
    }
}